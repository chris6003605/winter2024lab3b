import java.util.Scanner;
public class VirtualPetApp{
	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		Panda[] embarrassment = new Panda[4];
		
		for(int i =0; i < embarrassment.length; i++){
			embarrassment[i] = new Panda();
			System.out.print("What is panda " + (i+1) + " favorite food: ");
			embarrassment[i].food = input.nextLine();
			
			
			System.out.print("What is panda " + (i+1) + " size in CM: ");
			embarrassment[i].sizeInCM = Double.parseDouble(input.nextLine());
			
			
			System.out.print("What is panda " + (i+1) + " age? ");
			embarrassment[i].age = Integer.parseInt(input.nextLine());
			System.out.println();
			
		}
		System.out.println("The last Panda's favorite food is: " + embarrassment[embarrassment.length -1].food);
		System.out.println("The last Panda's size in CM is: " + embarrassment[embarrassment.length-1].sizeInCM);
		System.out.println("The last Panda's age is: " + embarrassment[embarrassment.length-1].age);
		System.out.println();
		
		embarrassment[0].printLifeSpan();
		embarrassment[0].eatBamboo();
	}
	
	
}