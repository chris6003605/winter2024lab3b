public class Panda{
	
	public String food;
	public double sizeInCM;
	public int age;
	
	public void printLifeSpan(){
		
		if(age >= 20){
			System.out.println("Your panda is dead or waiting for death");
		}
		else if( age > 10){
			System.out.println("Your panda is an full adult waiting to breed");
		}
		else{
			System.out.println("Your panda is a youngling");
		}
		
	}
	
	public void eatBamboo(){
		if(food.equalsIgnoreCase("Bamboo")){
			System.out.println("Your Panda is basic");
		}
		else if(food.equalsIgnoreCase("Leaves")){
			
			System.out.println("Your Panda is suffering! Give it Bamboo");
		}
		else{
			System.out.println("Your Panda has died");
		}
		
	}
}